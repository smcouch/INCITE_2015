\documentclass[12pt,titlepage]{article}

\setlength{\oddsidemargin}{0in}
\setlength{\evensidemargin}{0in}
\setlength{\textwidth}{6.5in}
%
\setlength{\textheight}{9in}
\setlength{\topmargin}{0in}
\setlength{\headsep}{0in}
\setlength{\topskip}{0in}
\setlength{\headheight}{0in}

\usepackage{graphicx}
\usepackage[square,comma,authoryear]{natbib}


\title{DOE Office of Science INCITE Project:\\
Petascale Simulation of Magnetorotational Core-Collapse Supernovae\\
Q3 2015 Report}

\author{Principal Investigator:\\Sean M. Couch\\
  Michigan State University}

\date{\today}

\begin{document}


\maketitle

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Report on Project Milestones}

%%%%%%%%%%%%%%%%%%%

Core-collapse supernovae (CCSNe) are the luminous explosions that
herald the death of massive stars.  Neutron stars, pulsars, magnetars,
and stellar-mass black holes are all born out of these explosions.
Some Gamma-Ray Bursts (GRBs) have been associated with CCSNe, raising
the possibility of a common progenitor for both.  CCSNe are
responsible for the production of many elements throughout the
universe, especially those heavier than iron; their importance in
galactic chemical evolution cannot be underestimated.  Despite the
importance of CCSNe to our understanding of many aspects of the
universe the mechanism that reverses stellar core collapse and drives
supernova explosions is not fully understood leaving our understanding
of many important astrophysical phenomena incomplete.  

This project is a comprehensive study of the impact of rotation and
magnetic fields on core-collapse supernovae. We will carry out a
series of 3D magnetohydrodynamics (MHD) simulations of the collapse of
rotating, magnetic stellar cores using the FLASH multi-physics,
adaptive mesh refinement (AMR) simulation framework.  We are including
in our simulations realistic treatments for neutrino physics,
realistic progenitor rotation and magnetic fields, and sufficient
resolution to capture the growth of the magnetorotational instability
(MRI).  We will use these simulations to quantify how much rotational
energy of the progenitor cores can be tapped to aid neutrinos in
driving successful and robust explosions. 



\subsection{Milestone: High-resolution Simulation of MHD CCSN Turbulence}


During Q3 we have continued our extremely-high resolution simulation of magnetorotational turbulence in CCSNe.  
This simulation has a finest grid spacing of 125 m.
Motivated by our recent work revealing the incredibly important role of even plain hydrodynamic turbulence, we have also started a second extremely high resolution simulation with no rotation or magnetic fields.
This simulation will serve as an incredibly valuable control case for the MHD simulation as well as being scientifically interesting in its own right as a test of the required resolution to accurately simulation turbulence in CCSNe.
We are now analyzing both simulations while concurrently continuing to run them to completion.  
We anticipate that continuing these simulations will consume our remaining allocation before the end of the year.

Analysis of the high-resolution MHD simulation has shown that, as we hypothesized in our proposal, the gain region behind the stalled supernova shock is unstable to the MRI.
Figure \ref{fig:mri110} shows the MRI stability condition (left) and wavelength of the fastest growing mode of the MRI (right) from a high-resolution 3D simulation of a mildly rotating, weakly magnetic progenitor.
This analysis shows that the wavelength of the fastest growing mode of the MRI roughly agrees with our original estimates and our high resolution simulations should be capable of resolving it.
What remains is to run these simulations for a sufficiently long period of time to see several MRI growth times (about 200 ms in total for the case in Figure \ref{fig:mri110}).

\begin{figure}
  \begin{tabular}{cc}
    \includegraphics[width=3.25in]{o110b9_C90_0566.png}
    \includegraphics[width=3.25in]{o110b9_FGM_0566.png}
  \end{tabular}
  \caption{
    Scaled MRI growth rate (left) and wavelength of the fastest growing mode (right) for a rotating progenitor with weak initial magnetic field strength at 140 ms post-bounce. 
    Here, negative (blue) regions indicate instability to the MRI while positive (orange) regions indicate stability.
  }
  \label{fig:mri110}
\end{figure}

 
We are also preparing a publication exploring the impacts of magnetic fields and rotation on the likelihood for explosion in 3D CCSN simulations and on the requirements for MHD jet formation.  
This paper presents results largely from the pilot simulations conducted as part of this INCITE project.
Are preliminary results indicate that either rotation or magnetic fields can aid explosions.
We argue this is due to the additional centrifugal support from rotation and/or the additional magnetic pressure.

\subsection{Milestone: Neutrino Transport Simulations}

We are very close to submitting a paper on our 2D full neutrino transport simulations using the two-moment M1 scheme.
The results of these 2D simulations are shown in Figure 1 where we
plot the shock radii for several different progenitor masses in the
purely-Newtonian limit (left panel) and for approximate GR (right
panel). 

\begin{figure}
  \begin{tabular}{cc}
    \includegraphics[width=3.in]{2D_NW_shockradius.pdf} 
    \includegraphics[width=3.in]{2D_GREP_shockradius.pdf}
  \end{tabular}
  \caption{Average shock radii versus time for 2D CCSN simulations
    using our explicit two-moment neutrino transport scheme.  Results
    for several different progenitor masses are shown.  The left panel
  shows results obtained using purely Newtonian gravity.  The right
  panel shows results that include an approximate treatment of general
relativistic gravity.}
\end{figure}

During Q3 we have extensively tuned our 3D M1 transport code for increased parallel performance.
We have introduced time sub-cycling of the radiation calculation in our code.
The explicit time step requirement of the M1 transport yields a time step size much much smaller than that required by hydro.
So now in the code we take two radiation steps for every one hydro step.
During the M1 sub-cycling step, we avoid any communication by evolving two layers of guard cells in addition to the interior cells.
This is possible because the M1 scheme uses only second-order spatial reconstruction, requiring just two layers of guard cells, where as our higher-order (third or fifth) hydro scheme requires four layers.
This has the effect of cutting the amount of communication per M1 step in half greatly improving the scaling of the M1 code and overall reducing the expense of the simulations significantly.

Because we decided to perform a second extremely high-resolution simulation of CCSN turbulence, we have decided to wait until Q1 2016 to carry out the planned 3D M1 simulations.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Project Productivity}
%%%%%%%%%%%%%%%%%%%

{\bf Publications}
\begin{itemize}

\item O'Connor, E.P, Couch, S.M. 2015, ``Influence of General Relativity on Multidimensional Simulations of Core-collapse Supernovae,'' to be submitted to
  {\it Astrophysical Journal}

\item Couch, S.M., Ott, C.D. 2015, ``The Role of Turbulence in
  Neutrino-driven Core-collapse Supernova Explosions,'' {\it
    Astrophysical Journal}, 799, 5

\item Radice, D., Couch, S.M., Ott, C.D. 2015, ``Implicit Large Eddy
  Simulations of Anisotropic Weakly Compressible Turbulence with
  Application to Core-collapse Supernovae,'' {\it
    Computational Astrophysics and Cosmology}, 2, 7

\item Couch, S.M., Chatzopoulos, E., Arnett, W.D., Timmes, F.X., ``The
  Three Dimensional Evolution to Core Collapse of a Massive Star,''
  {\it Astrophysical Journal Letters}, 808, L21

\item Radice, D., Ott, C.D., Abdikamalov, E., Couch, S.M., Haas, R., Schnetter, E. 2015, ``Neutrino-driven Convection in Core-collapse Supernovae: High-resolution Simulations,'' submitted to {\it Astrophysical Journal}, arXiv:1510.05022

\end{itemize}

\noindent {\bf Presentations}

\begin{itemize}

\item Couch, S.M., ``Simulations of Supernovae and Their Massive Star Progenitors in 3D,''
  Physics Division Seminar, Oak Ridge National Laboratory, 29 September 2015

\item Couch, S.M., ``Core-collapse Supernovae as Nuclear Physics Laboratories,'' Nuclear Science Seminar, National Superconducting Cyclotron Laboratory, 1 October 2015

\end{itemize}

\noindent {\bf Press}

\begin{itemize}

\item ``Stellar Death in Three Dimensions,''
  Michigan State University/Joint Institute for Nuclear
  Astrophysics Press release 

\end{itemize}


\section{Code Description and Characterization}

\texttt{FLASH} is a highly capable, fully modular, extensible, community
code that is widely used in astrophysics, cosmology, fluid dynamics, and
plasma physics, and other fields.  The capabilities of the FLASH code
include adaptive mesh refinement (AMR), several self-gravity solvers, an
advection-diffusion-reaction (ADR) flame model, an accurate and
detailed treatment of nuclear burning, and a sophisticated two-moment
neutrino transport scheme based on an explicit hyperbolic solver.

\texttt{FLASH} is written in modern Fortran, with some utility functions
written in C, and a build system written in Python.  It requires MPI
library support, and either HDF5 or P-NetCDF for I/O.  Additional
mathematical, such as \texttt{Hypre}, may be required to configure
\texttt{FLASH} for particular simulations.

Algorithm classes used within \texttt{FLASH} include Sparse Linear Algebra
solvers, FFT, active and passive particles, structured grids, and AMR.

\end{document}
