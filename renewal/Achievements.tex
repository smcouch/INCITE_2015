\documentclass[12pt]{article}
\usepackage{geometry}                % See geometry.pdf to learn the layout options. There are lots.
\geometry{letterpaper}                   % ... or a4paper or a5paper or ... 
%\geometry{landscape}                % Activate for for rotated page geometry
%\usepackage[parfill]{parskip}    % Activate to begin paragraphs with an empty line rather than an indent
\usepackage{graphicx}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{epstopdf}
\usepackage{wrapfig}
\usepackage{natbib}
%\usepackage[square,comma,numbers,sort]{natbib}
\usepackage[pdftex, plainpages=false, colorlinks=true, linkcolor=blue, citecolor=blue, bookmarks=false]{hyperref}
\usepackage{setspace}
\usepackage{multicol}
\usepackage{sectsty}
\usepackage{url}
\usepackage{lipsum}
\usepackage{times}
\usepackage[tiny,compact]{titlesec}
\usepackage{fancyhdr}
\usepackage{deluxetable}
\usepackage[font=footnotesize,labelfont=bf]{caption}
\usepackage{verbatim}
\usepackage{acronym}

\setlength{\textwidth}{6.5in}
\setlength{\oddsidemargin}{0.0cm}
\setlength{\evensidemargin}{0.0cm}
\setlength{\topmargin}{-0.5in}
\setlength{\headheight}{0.2in}
\setlength{\headsep}{0.2in}
\setlength{\textheight}{9.in}
%\setlength{\footskip}{-0.2in}
%\setlength{\voffset}{0.0in}


\sectionfont{\normalsize}
\subsectionfont{\normalsize}
\subsubsectionfont{\normalsize}
\singlespacing

\pagestyle{fancy}
\fancyhf{} 
\lhead{\fancyplain{}{Petascale Simulation of Magnetorotational Core-Collapse Supernovae}}
\rhead{\fancyplain{}{S.M. Couch}}
\rfoot{\fancyplain{}{\thepage}}

\bibliographystyle{apj}
%\bibliographystyle{hapj}
%\bibliographystyle{physrev}


\input macros.tex
\input journal_abbr.tex

%\titlespacing*{\section}{0in}{0.2in}{0in}
%\titlespacing*{\subsection}{0in}{0.1in}{0in}
\titleformat*{\subsection}{\itshape}
%\titlespacing*{\subsubsection}{0in}{0.in}{0in}
\titleformat*{\subsubsection}{\itshape}
\setlength{\abovecaptionskip}{3pt}

\begin{document}

\acrodef{CCSNe}{core-collapse supernovae}
\acrodef{CCSN}{core-collapse supernova}

\begin{center}
{\bf Year 2 - CY2016 Allocation Renewal} \vspace{-0.1in}
\end{center}

\section*{Achievements Thus Far in Year 1}



\section{Importance of Turbulence in the CCSN Mechanism}


\begin{figure}[!b]
  \begin{tabular}{ccc}
    \includegraphics[width=2.1in,trim= 5.in 3in 4in 5in ,clip] {../figures/s15_n5m2_hf102_volRend_549}
    \includegraphics[width=2.1in,trim= 2.9in .4in .2in 0.5in ,clip]{../figures/m25_o220b12_entr}
    \includegraphics[width=2.1in,trim= 3.75in .4in .2in 0.5in ,clip]{../figures/m25_o220b12_beta2}
  \end{tabular}
  \caption{
    Volume renderings from 3D simulations accomplished using this project's discretionary allocation on {\it Mira}.
    The left panel shows entropy from the successful explosion triggered by pre-collapse progenitor asphericity \citep{Couch:2013b}.
    The middle and right panels are from the same MHD CCSN simulation being run now.
    The middle panel shows entropy and the right panel shows plasma $\beta$, highlighting regions of strong magnetic field.
    This simulation includes rapid rotation and strong magnetic fields that result in MHD jet production.
    These jets become unstable in a non-axisymmetric manner similar to the results of \citet{Mosta:2014}.
  }
  \label{fig:discSims}
\end{figure}

Early in 2015, \citet{Couch:2015} showed the incredible importance of aiding neutrino-driven CCSN explosions.
Based on a large set of high-resolution, 3D CCSN simulations carried out exclusively on {\it Mira}, \citeauthor{Couch:2015} demonstrated that what separates successful explosions from failures is a strong {\it turbulent pressure} behind the shock.
In successful explosions, this turbulent pressure could be in excess of 50\% of the background thermal pressure, making a very large contribution to the stability of the standing supernova shock, a possibility first pointed out by \citet{Murphy:2013}.
Furthermore, \citet{Couch:2015} make a direct connection between the presence of strong non-spherical motion in the pre-collapse progenitor star and the strength of the turbulence in the layer behind the stalled supernova shock, providing a solid physical explanation for why such progenitor asphericity can trigger successful explosions \citep{Couch:2013b}.
This work pointed out, in the clearest way yet, the importance of turbulence in the supernova mechanism and, thus, goes directly to the heart of our INCITE program.
The key aspect of our INCITE program is to treat the turbulence, particularly the magnetohydrodynamic turbulence, with the highest fidelity yet achieved.
The work of \citeauthor{Couch:2015} makes the mission of our INCITE program all the more imperative as it is now irrefutable that turbulence plays a key role in the CCSN mechanism.
An example of a successful explosion triggered by progenitor asphericity is shown in the left panel of Figure \ref{fig:discSims}.


\citet{Couch:2015} raised a number of questions, chief amongst them being How well are we treating the post-shock turbulence in the CCSN context?
The observed {\it numerical} Reynolds numbers found in their simulations (around a few hundred) were significantly below what would be expected for the {\it physical} Reynolds numbers ($\sim 10^{17}$).
This is concerning because if the numerical Reynolds numbers are too low, the flow will not behave in a truly turbulent fashion, i.e., the dynamics will be incorrect.  
Given the newfound insight that turbulence is so crucial to the CCSN mechanism, this possibility is a great cause for concern.
The principle reason that the numerical Reynolds numbers as so low is insufficient resolution.
In order to ascertain what resolution might be required to accurately model CCSN turbulence with the typical methods used in such simulations, \citet*{Radice:2015} performed a resolution study with FLASH of anisotropic driven turbulence under the conditions typical of the CCSN post-shock region using implicit large eddy methods (such as are used in all CCSN simulations to-date).
\citeauthor*{Radice:2015} found that even the {\it best} resolution used in multidimensional global simulations of CCSNe is about {\it a factor of ten} too coarse to accurately model the turbulent energy cascade.
Furthermore, they found that an incorrect turbulent energy cascade can affect the effective turbulent pressure on large scales and, hence, the stability of the supernova shock in global simulations.
This underscores the necessity to correctly model the turbulence in global simulations of the CCSN mechanism.
While these simulations were not carried out using INCITE resources, they speak directly to the necessity of the simulations we are carrying out as part of this project.



\section{Three Dimensional CCSN Progenitor Simulation}

\citet{Couch:2013b} and \citet{Couch:2015} showed the remarkable importance of non-spherical structure in CCSN progenitors.
Historically, 1D spherically-symmetric stellar progenitor models have been used exclusively as the initial conditions for CCSN simulations.
\begin{wrapfigure}[18]{r}{3.25in}
  \includegraphics[width=3.25in]{supernova_progenitor_3D_velocity2.png}
  \caption{Radial velocity from the 3D CCSN progenitor simulation of \citet{Couch:2015a} just prior to iron core collapse.}
  \label{fig:progen}
\end{wrapfigure}
Real stars, importantly, are not spherically-symmetric, however, and 1D stellar evolution models account for this with various approximate ``fixes'' such as mixing length theory to model convection.
Just prior to iron core collapse in massive stars, energetic silicon burning in a shell surrounding the iron core can drive violent large scale convective motions \citep{Arnett:2011}.
This fact motivated the parametric studies of \citet{Couch:2013b} and \citet{Couch:2015}, but the realistic 3D character of the silicon burning-driven asphericity surrounding the collapsing iron core remained an open question.
To address this directly, in \citet{Couch:2015a} we present the first ever successful 3D simulation of the final minutes of massive stellar evolution all the way through the point of iron core collapse.
This simulation was not particularly expensive, using only a very small portion of our INCITE allocation, but required several technical numerical advances such as a new nuclear network in FLASH and a tighter coupling between the nuclear energy generation rate and the hydrodynamics.


Figure \ref{fig:progen} shows the radial velocity from this 3D progenitor simulation just prior to iron core collapse. 
We find convective speeds in excess of 300 km/s surrounding the iron core.
In \citet{Couch:2015a} we go on to show that, compared to 1D spherically-symmetric initial conditions, the presence of such aspherical motions in the progenitor is significantly favorable for the success of the neutrino mechanism.
The reason for this is precisely the arguments made by \citet{Couch:2015} that non-spherical structure in the progenitor star results in stronger post-shock turbulence and, hence, and greater turbulent pressure to aid in shock expansion.


\section{Parameter Study of Magnetorotational CCSNe}


\begin{figure}
  \begin{tabular}{cc}
    \includegraphics[width=3.25in]{o220b12_C90_0381.png}
    \includegraphics[width=3.25in]{o220b12_FGM_0381.png}
  \end{tabular}
  \caption{Scaled MRI growth rate (left) and wavelength of the fastest growing mode (right) for a rapidly rotating progenitor with strong initial magnetic field strength at 40 ms post-bounce.  }
  \label{fig:mri220}
\end{figure}

\begin{figure}
  \begin{tabular}{cc}
    \includegraphics[width=3.25in]{o110b9_C90_0566.png}
    \includegraphics[width=3.25in]{o110b9_FGM_0566.png}
  \end{tabular}
  \caption{Same as Figure \ref{fig:mri220} but for moderate rotation and weak initial magnetic field at 140 ms post-bounce.}
  \label{fig:mri110}
\end{figure}

One of the primary goals for Year 1 of this INCITE project was to complete a parameter study of magnetorotational CCSN simulations at moderate resolution in order to assess the conditions for magnetic field growth, as well as MHD jet formation.
These simulations would serve as the pathfinders for the extremely high-resolution MHD simulation with sufficient resolution to resolve the fastest-growing mode of the MRI, as well as being scientifically interesting in their own right.
During the first half of 2015, we have carried out such a study in a 25 $M_\odot$ progenitor from \citet{Heger:2005}.
We have varied the initial rotation rate and the initial magnetic field strength.
Our study includes controls of no rotation and zero magnetic fields.
We also include both plausible rotation/field strengths as well as rapid rotation and strong fields.
One of the key results of these simulations is the conditions under which MHD-dominated outflows (``jets'') from the proto-neutron star (PNS) can form.
We find that both rapid rotation and strong initial fields are needed for jet formation.
We are continuing a simulation with rapid rotation but weak initial fields to see if magnetic field amplification over long post bounce times will eventually lead to jet formation.
An example of one of our CCSN simulations that results in MHD jet formation is shown in Figure \ref{fig:discSims}.
The initial results of this parameter study are currently in preparation for submission.

A key result of these simulations for the INCITE project is an exploration of the conditions under which the MRI grows and the scales on which the MRI grows fastest.
In Figures \ref{fig:mri220} and \ref{fig:mri110} we show an analysis of the conditions for MRI growth in two of our simulations.
The left halves of the figures show the scaled MRI growth rates: negative values (blue colors) represent unstable regions while positive values (yellow colors) represent regions stable to the MRI.
The right halves of the figures show the wavelength of the fastest-growing mode of the MRI.
In this analysis, we have used the full dispersion relation for the MRI which includes dependence on the Brunt-Vaisaila frequency, i.e., buoyancy \citep{Obergaulinger:2009}. 
Unlike the case of accretion disks, non-negligible entropy and composition gradients can significantly alter the MRI stability of collapsing stellar cores.
In Figures \ref{fig:mri220} and \ref{fig:mri110} this is obvious from the highly complex spatial dependence of the MRI growth rates.

Figure \ref{fig:mri220} shows a model with rapid rotation and strong initial magnetic field strength.
Indeed, the post-collapse magnetic fields reach levels that we might expect from a fully saturated MRI ($\sim10^{15}$~G) just from flux compression during collapse.
Since the wavelength of the MRI FGM increases with field strength, for this model the MRI is resolved even in this simulation with finest grid spacing of 0.49 km.
For less rapidly rotating and weaker field initial conditions, the wavelength of the MRI FGM is significantly shorter, as seen in Figure \ref{fig:mri110}.
This is especially true at early post-bounce times, but we find that at later times as the magnetic field is amplified from rotational winding and the convective dynamo that the wavelength of the MRI increases, as expected.
By 140 ms post-bounce (shown in Figure \ref{fig:mri110}), the MRI FGM is around 1 km.
We find that even for the slower rotation and weak fields, much of the entire post-shock region is unstable to the MRI, though with complex dependence on space and time.

\section{Direct Numerical Simulations of CCSN Turbulence}




The vast majority of resource allocation so far this year has been spent on extremely high resolution simulations targeted at resolving the MRI and accurately modeling the post-shock turbulence.
As discussed in the original proposal, in order to make this feasible we place the highest resolution elements only outside of the PNS, where the sound speeds are restrictively high and the flow is stable to the MRI.
Figure \ref{fig:grid} shows an example of this grid structure.
The left half of the figure shows the grid fo a fiducial-resolution simulation with finest grid spacing of 0.49 km while the right half shows how we add additional levels of refinement outside the PNS to achieve, in this case, resolution of 0.123 km.

\begin{wrapfigure}[23]{r}{3.25in}
  \includegraphics[width=3.2in]{plasmaBeta_o110b9_125mV500m_grid_0393_cropped.png}
  \caption{Comparison of AMR grid structures from our fiducial resolution simulations (left) and our high resolution simulations (left).}
  \label{fig:grid}
\end{wrapfigure}
We are currently running two full-star simulations with finest grid spacing of 0.123 km outside of the PNS, one non-rotating with zero initial magnetic field and one with moderate rotation and weak initial field strength.
This is a slight re-scoping of the original goals laid out in the INCITE proposal wherein we planned one single MHD simulation with even higher resolution but in an octant geometry.
We decided on this minor correction for several reasons.
First, testing showed that octant geometry introduced undesirable artifacts especially along the equator of the star where the MRI grows the fastest.
We explored the use of the ``quadrant'' geometry where no symmetry would be enforced at the equator but we found that, due to the extra communication needed for the rotating boundary conditions, we did not realize a factor of four speed-up relative to the full star simulations.  
Additionally, again due to the extra communication, scaling was not as good for the quadrant geometry.
Thus we opted for the full star without any enforced symmetry.
Another important factor in our re-scoping the project goals was that our initial allocation request of 100M SUs was cut in half.
Finally, given the recent realization of how important plain hydrodynamic turbulence is in the CCSN mechanism, we decided that a non-magnetic, non-rotating ``control'' simulation was crucial.
This simulation will be valuable not only as a comparison to the magnetorotational simulation but also as a test of the resolution required to accurately model hydrodynamic turbulence in the CCSN context.

Both simulations are still running and we expect the majority of our remaining allocation (30M SUs) will go toward finishing them.
Already we are seeing indications that, for the magnetorotational simulation, the growth rate of the magnetic field strength is faster than for the lower resolution case, all else being equal.
Detailed analysis of both simulations will be carried out in the second half of this year.



\section{2D M1 Simulations and Performance Tuning of 3D M1 Code}

The other major goal of our original proposal was to conduct 3D full-neutrino transport simulations using an explicit two-moment scheme with an analytic closure for higher-order moments (``M1'').
Despite the reduction in our allocation request, we have still charged ahead with this effort in hopes of running these simulations in the backfill queue. 
The major milestone for the M1 simulations during the first half of 2015 was to tune the code for better parallel performance.
Compared to the neutrino leakage variant of our code, the M1 version did not scale as well on {\it Mira} (see Figure \ref{fig:scaling}) due to the tremendous amount of extra data that must be communicated ($\sim$~150 extra grid variables per zone!).
We have taken two approaches to improving performance.
First, we increased the size of the adaptive mesh refinement unit block from $16^3$ to $24^3$.  
This has the important effect of decreasing the ratio of guard cells to interior cells of the block, thus reducing the amount of communication relative to computation per block per step.
In the process, we tuned the code to achieve better overall OpenMP thread-to-thread speedup.



\begin{wrapfigure}[19]{l}{3.25in}
  \includegraphics[width=3.25in]{s25WH07_M1_2D_0566.png}
  \caption{Radial velocity (left) and specific entropy (right) from a 2D simulation of a 25 $M_\odot$ star using our M1 neutrino transport scheme. }
  \label{fig:m1}
\end{wrapfigure}
The second major step we have taken is the introduce time sub-cycling of the radiation calculation in our code.
The explicit time step requirement of the M1 transport yields a time step size much much smaller than that required by hydro.
So now in the code we take two radiation steps for every one hydro step.
During the M1 sub-cycling step, we avoid any communication by evolving two layers of guard cells in addition to the interior cells.
This is possible because the M1 scheme uses only second-order spatial reconstruction, requiring just two layers of guard cells, where as our higher-order (third or fifth) hydro scheme requires four layers.
This has the effect of cutting the amount of communication per M1 step in half greatly improving the scaling of the M1 code and overall reducing the expense of the simulations significantly.

We are now ready to proceed with full-scale 3D M1 simulations.
But in addition to the tuning efforts of our M1 code during 2015, we have also carried out a 2D M1 simulation campaign using several progenitor masses.
This study, while useful in informing what progenitors and parameters should be used for the 3D simulations, has also yielded very interesting results in its own right.
In particular, we have investigated the impact of using the so-call ``pseudo-general relativistic'' gravitational potential \citep{Marek:2006}.
This potential models the most important effects of GR in an otherwise Newtonian calculation.
We find the pseudo-GR potential has a very significant impact on our CCSN simulations.
For purely Newtonian gravity, without the pseudo-GR potential, we find NO successful 2D explosions across a range of progenitor masses from 12 $M_\odot$ to 30 $M_\odot$.
A similar conclusion was reached by the purely-Newtonian simulations of \citet{Dolence:2015}.
Including the pseudo-GR potential, and concomitant GR terms in the neutrino transport, results in explosions for a few progenitors, but not all, in close agreement with the results from the Garching group \citep[e.g.][]{Muller:2012a}.
This work is being written up for publication now.
An example of our 2D M1 results is shown in Figure \ref{fig:m1}.


\section{Allocation Use}


\begin{figure}
  \begin{tabular}{cc}
    \includegraphics[width=3.25in]{on_track_graph.png}
    \includegraphics[width=3.25in]{categorized_hours_graph.png}
  \end{tabular}
  \caption{Allocation usage.}
  \label{fig:usage}
\end{figure}


To-date we have received roughly 40\% percent of our original 2015 allocation, or about 20M SUs on {\it Mira}.
Our allocation usage summary plots are shown in Figure \ref{fig:usage}.
The left panel of Figure \ref{fig:usage} shows our historical allocation usage compared with the ``on-track'' line.
Our project usage is slightly behind the on-track line, but the majority of our usage came within in about a month-long span in April during which we ran the first half of our production simulations.
In May, the PI relocated from California to Michigan, precipitating a slow down in usage.\footnote{This relocation was in order to start as an Assistant Professor at Michigan State University.  The PI is convinced that the awarding of this INCITE allocation played an important positive role in his obtaining a tenure-track position and is, therefore, very grateful to DOE Leadership Computing and ALCF.}
But we are now again ready for production runs and expect to very quickly burn through our remaining allocation over the course of about two months.  
We expect we will be in an overburn situation long before then end of the calendar year.

The right half of Figure \ref{fig:usage} shows the breakdown of our allocation usage in scale categories.
So far this year, about 80\% of our total usage has been at the ``capability'' scale of between 16.7\% and 33.3\% of {\it Mira's} total available production resources.

\section{Application Parallel Performance}


In Figure \ref{fig:scaling} we reproduce our scaling tests from the original proposal.
Our benchmarks use the full, production version of FLASH, including MHD, self-gravity, AMR, neutrino leakage/M1 neutrino transport, and nuclear equation of state. 
Figure \ref{fig:scaling} shows the weak scaling on {\it Mira} for FLASH going up to 32,768 nodes (524k cores) for leakage and 8192 nodes (128k cores) for M1. 
The leakage variant scales almost perfectly from 512 to 32,768 nodes.  
Our M1 version of FLASH does not scale as efficiently because it requires a great deal more communication since the M1 calculation needs over 150 extra grid variables than the leakage does.
As discussed in Section 5 above, we have greatly increased the communication performance of our M1 code and we are now working on a replacement weak scaling study.
As is to be expected, IO does not scale perfectly.
Despite the exponential growth in the time required for IO, it is still only a small fraction ($\sim5\%$) of the total runtime during production simulations since IO occurs so infrequently.  
We also plot in Fig. \ref{fig:scaling} the strong scaling and thread-to-thread speedup for FLASH.  
Both are quite acceptable for capability-level production simulations.


In our actual production simulations thus far this year, we achieve performance very comparable to that shown in Figure \ref{fig:scaling}.
For our MHD simulations, we are seeing in production capability simulations usage rates closer to $1.5\times10^{-7}$ core-hours per zone per step, as opposed to the $1.0\times10^{-7}$ core-hours per zone per step shown in Figure \ref{fig:scaling}.
This is because we have introduced extra equation of state (EOS) calls into our Riemann solver.
We found this to be beneficial to numerical stability of our code, especially for rapidly rotating and strong magnetic field cases.
\begin{wrapfigure}[41]{r}{3.5in}
  \begin{tabular}{c}
    \includegraphics[width=3.5in, trim= 0.13in 0in 0.in 0.in,clip]{../data/wkScaling2} \\
    \includegraphics[width=3.5in, trim= 0.13in 0in 0in .25in,clip]{../data/strScaling} \\
    \includegraphics[width=3.5in, trim= 0.13in 0.in 0.in .25in,clip]{../data/thrSpeedup}
  \end{tabular}
  \caption{Weak scaling (top), strong scaling (middle), and threading
    speedup (bottom) of FLASH-MaRCC1 on {\it Mira}.  Note that for this configuration, use of 8 threads can result in a maximum theoretical speedup of 4x.}
  \label{fig:scaling}
\end{wrapfigure}   
The downside is the requirement for eight additional EOS calls per zone per step, each of which involves multiple table lookups in an iterative solution scheme.



\section{Data Storage}

Presently, we are using 122 TB of online storage on {\it Mira} of an allocated 200 TB.  
We expect we will fill this allocation before the end of the year and are working on offloading to tape non-critical data.
Overall, our estimated data usage per simulation has been accurate.



\renewcommand\bibsection{\section*{References}}
\setlength{\bibsep}{2pt}
%\begin{multicols}{2}
\bibliography{Achievements}
%\end{multicols}




\end{document}