#!/usr/bin/env python                                                                                                                                                                  

from numpy import *
from pylab import *
from string import *
from matplotlib.ticker import AutoMinorLocator

nodes, blocks, evol, init, hydro, grav, plt, grid = loadtxt('wkPerf2.dat', usecols=(0,1,2,3,4,5,7,8), unpack=True)
nodes2, blocks2, evol2, init2, hydro2, grav2, plt2, grid2, leak = loadtxt('wkPerf_uhd.dat', usecols=(0,1,2,3,4,5,7,8,9), unpack=True)
nodes3, blocks3, evol3 = loadtxt('m1Scaling.dat', usecols=(0,1,2), unpack=True)

steps = 50.
nxb = 24.

cstEvol = nodes*16*evol/3600./(blocks*nxb**3*steps)*1e8
cstHydr = nodes*16*hydro/3600./(blocks*nxb**3*steps)*1e8
#cstInit = cst
cstGrav = nodes*16*grav/3600./(blocks*nxb**3*steps)*1e8
cstPlot = nodes*16*plt/3600./(blocks*nxb**3)*1e7
cstGrid = nodes*16*grid/3600./(blocks*nxb**3*steps)*1e8

steps = 20.
nxb = 16.

cstLeak = nodes2*16*leak/3600./(blocks2*nxb**3*steps)*1e8
cstM1   = nodes3*16*evol3/3600./(blocks3*nxb**3*steps)*1e8

# Add leakage to total MHD
cstEvol = cstEvol + cstLeak

figure(1,figsize=(5,3.5))
rc('lines',markeredgewidth=1.)
rc('text', usetex=True)
rc('font', family='serif', size=12)
ax1 = subplot(111)
ax2 = ax1.twinx()

#ax1.semilogx()
#ax2.semilogx()
#ax1.loglog()
#ax2.loglog()

minorLocatorX   = AutoMinorLocator()
minorLocatorY   = AutoMinorLocator()
minorLocatorY2   = AutoMinorLocator()

lines = []
lines.extend(ax1.plot(nodes, cstEvol, 'b-',linewidth=1.5))
#lines.extend(ax1.plot(nodes, cstHydr, 'r-',linewidth=1.5))
lines.extend(ax1.plot(nodes3, cstM1, 'r-',linewidth=1.5))
#lines.extend(ax1.plot(nodes, cstInit, 'g-'))
#lines.extend(ax1.plot(nodes, cstGrav, 'm-',linewidth=1.5))
#lines.extend(ax1.plot(nodes, cstGrid, 'c-',linewidth=1.5))
lines.extend(ax2.plot(nodes, cstPlot, 'g-',linewidth=1.5))
#lines.extend(ax1.plot(nodes2, cstLeak, 'y-',linewidth=1.5))
ax1.plot(nodes, cstEvol, 'bs')
ax1.plot(nodes3, cstM1, 'rs')
#ax1.plot(nodes, cstInit, 'gs')
#ax1.plot(nodes, cstGrav, 'ms')
#ax1.plot(nodes, cstGrid, 'cs')
ax2.plot(nodes, cstPlot, 'gs')
#ax1.plot(nodes2, cstLeak, 'ys')
lines.extend(ax1.plot((512,32768), (cstEvol[0],cstEvol[0]), 'k--',linewidth=1.5))
ax1.plot((512,32768), (cstEvol[0],cstEvol[0]), 'b--',linewidth=1.5)
ax1.plot((512,8192), (cstM1[0],cstM1[0]), 'r--',linewidth=1.5)
#ax1.plot((512,2048), (cstHydr[0],cstHydr[0]), 'k--')
#ax1.plot((512,2048), (cstInit[0],cstInit[0]), 'k--')

ax1.set_xscale('log')
ax2.set_xscale('log')
ax1.set_xticks([512,1024,2048,4096,8192,16384,32768])
ax2.set_xticks([512,1024,2048,4096,8192,16384,32768])
ax1.get_xaxis().set_major_formatter(matplotlib.ticker.ScalarFormatter())
ax2.get_xaxis().set_major_formatter(matplotlib.ticker.ScalarFormatter())
ax1.set_xticklabels(["512\n(4k)", "1024\n(8k)", "2048\n(16k)", "4096\n(32k)", "8192\n(64k)", "16384\n(128k)", "32768\n(256k)"])
ax1.minorticks_off()
#ax2.set_xticks([512,1024,2048,4096,8192,16384])
#ax1.tick_params(width=1.,which='both')
#ax1.xaxis.set_minor_locator(minorLocatorX)
ax1.yaxis.set_minor_locator(minorLocatorY)
ax2.yaxis.set_minor_locator(minorLocatorY2)

ax1.set_ylim((0,70.))
ax1.set_xlim((300, 40000))

ax1.set_xlabel(r'Mira nodes (ranks), 8 threads/rank')
ax1.set_ylabel(r'$10^{-8}$ core hours/zone/step')
ax2.set_ylabel(r'$10^{-7}$ core hours/zone/IO write')
#ax1.set_xlabel('Mira nodes (ranks) (16 cores/node)')
#ax1.set_ylabel('1e-8 core-hours/zone/step')

subplots_adjust(bottom=0.18,top=.95)
gca().xaxis.set_label_coords(0.5,-0.08)

#lab1 = r'${\rm Total evolution}$'
#lab2 = r'${\rm Hydro}$'
lab1 = 'FLASH 3D Leakage'
lab2 = 'FLASH 3D M1'
lab3 = 'Gravity'
lab4 = 'Grid'
lab5 = 'IO (1 write)'
lab6 = r'$\nu$-Leakage'
lab7 = 'Ideal'
legend(lines,(lab1,lab2,lab5,lab7), 'center left',labelspacing=0.05,handletextpad=0.)
leg = gca().get_legend()
leg.draw_frame(False)
ltext = gca().get_legend().get_texts()
setp(ltext, fontsize=10)

savefig("wkScaling2.pdf")
