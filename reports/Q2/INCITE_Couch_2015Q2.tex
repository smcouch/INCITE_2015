\documentclass[12pt,titlepage]{article}

\setlength{\oddsidemargin}{0in}
\setlength{\evensidemargin}{0in}
\setlength{\textwidth}{6.5in}
%
\setlength{\textheight}{9in}
\setlength{\topmargin}{0in}
\setlength{\headsep}{0in}
\setlength{\topskip}{0in}
\setlength{\headheight}{0in}

\usepackage{graphicx}
\usepackage[square,comma,authoryear]{natbib}


\title{DOE Office of Science INCITE Project:\\
Petascale Simulation of Magnetorotational Core-Collapse Supernovae\\
Q1 2015 Report}

\author{Principal Investigator:\\Sean M. Couch\\
  Michigan State University}

\date{July 1, 2015}

\begin{document}


\maketitle

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Report on Project Milestones}

%%%%%%%%%%%%%%%%%%%

Core-collapse supernovae (CCSNe) are the luminous explosions that
herald the death of massive stars.  Neutron stars, pulsars, magnetars,
and stellar-mass black holes are all born out of these explosions.
Some Gamma-Ray Bursts (GRBs) have been associated with CCSNe, raising
the possibility of a common progenitor for both.  CCSNe are
responsible for the production of many elements throughout the
universe, especially those heavier than iron; their importance in
galactic chemical evolution cannot be underestimated.  Despite the
importance of CCSNe to our understanding of many aspects of the
universe the mechanism that reverses stellar core collapse and drives
supernova explosions is not fully understood leaving our understanding
of many important astrophysical phenomena incomplete.  

This project is a comprehensive study of the impact of rotation and
magnetic fields on core-collapse supernovae. We will carry out a
series of 3D magnetohydrodynamics (MHD) simulations of the collapse of
rotating, magnetic stellar cores using the FLASH multi-physics,
adaptive mesh refinement (AMR) simulation framework.  We are including
in our simulations realistic treatments for neutrino physics,
realistic progenitor rotation and magnetic fields, and sufficient
resolution to capture the growth of the magnetorotational instability
(MRI).  We will use these simulations to quantify how much rotational
energy of the progenitor cores can be tapped to aid neutrinos in
driving successful and robust explosions. 



\subsection{Milestone: High-resolution Simulation of MHD CCSN Turbulence}


Guided by the pilot simulations performed during Q1, in Q2, we
have run the first production simulation to study MHD turbulence in
the CCSN environment at extremely-high resolution.  Using the
sophisticated gridding techniques enable by our code's flexible AMR
package, we achieved a finest resolution element of 125 m, roughly a
factor of three better than any 3D CCSN simulation ever performed.
This simulation ran for several rotation periods of the proto-neutron
star.  This campaign consumed over 20 million service units on Mira,
roughly 40\% of our allocation.

We are now in the process of analyzing this simulation, along with the
pilot simulation.  We are specifically interested in the growth rates
of the MHD instabilities, such as the magnetorotational instability.
Additionally, we are addressing the impact of rotation and magnetic
fields on the favorability for successful supernova explosions.

\subsection{Milestone: Neutrino Transport Simulations}

During Q2, we have also carried out a large set of 2D CCSN simulations
using our explicit two-moment neutrino transport scheme that will be
used in our planned 3D simulations.  These simulations will serve as
invaluable guideposts in the design of the production 3D simulations
that we will execute during the Q3 and Q4.  In their own right, they
have yielded significant scientific results.  Specifically, these 2D
simulations we have conducted as part of this INCITE project have
shown that inclusion of general relativistic effects has a very
significant qualitative impact on the results of the simulations.  For
the purely Newtonian cases, we find that {\it no} progenitor
successfully explodes, while including an approximate treatment of
relativistic gravity results in successful explosions for some
progenitors.  This has the potential, then, the resolve tension
amongst recent 2D CCSN simulation results from different research
groups.

The results of these 2D simulations are shown in Figure 1 where we
plot the shock radii for several different progenitor masses in the
purely-Newtonian limit (left panel) and for approximate GR (right
panel). 

\begin{figure}
  \begin{tabular}{cc}
    \includegraphics[width=3.in]{2D_NW_shockradius.pdf} 
    \includegraphics[width=3.in]{2D_GREP_shockradius.pdf}
  \end{tabular}
  \caption{Average shock radii versus time for 2D CCSN simulations
    using our explicit two-moment neutrino transport scheme.  Results
    for several different progenitor masses are shown.  The left panel
  shows results obtained using purely Newtonian gravity.  The right
  panel shows results that include an approximate treatment of general
relativistic gravity.}
\end{figure}

Guided by the results of these 2D simulations, during Q3 we will
execute two 3D simulations using full neutrino transport.  In order to
verify the impact of GR gravity that we have found in 2D, one 3D
simulation will include approximate GR effects while the other will be
purely Newtonian.  The simulation will be carried out using a 25 solar
mass progenitor that explodes robustly in 2D when GR is included yet
fails for the Newtonian limit.  We anticipate that the vast majority
of our remaining allocation will be spent on these simulations. 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Project Productivity}
%%%%%%%%%%%%%%%%%%%

{\bf Publications}
\begin{itemize}

\item O'Connor, E.P, Couch, S.M. 2015, ``Two Dimensional Core-collapse
  Supernova Simulations with Explicit Two-moment Neutrino Transport:
  the Impact of General Relativistic Gravity,'' to be submitted to
  {\it Astrophysical Journal}

\item Couch, S.M., Ott, C.D. 2015, ``The Role of Turbulence in
  Neutrino-driven Core-collapse Supernova Explosions,'' {\it
    Astrophysical Journal}, 799, 5

\item Radice, D., Couch, S.M., Ott, C.D. 2015, ``Implicit Large Eddy
  Simulations of Anisotropic Weakly Compressible Turbulence with
  Application to Core-collapse Supernovae,'' to appear in{\it
    Computational Astrophysics and Cosmology}, arXiv:1501:3169

\item Couch, S.M., Chatzopoulos, E., Arnett, W.D., Timmes, F.X., ``The
  Three Dimensional Evolution to Core Collapse of a Massive Star,''
  to appear in {\it Astrophysical Journal Letters}, arXiv:1503.2199

\end{itemize}

\noindent {\bf Presentations}

\begin{itemize}

\item Couch, S.M., ``Turbulent Frontiers in Massive Stellar Death,''
  Astrophysics Colloquium, Caltech, Pasadena, CA,
  April 21, 2015

\end{itemize}

\noindent {\bf Press}

\begin{itemize}

\item ``Stellar Death in Three Dimensions,''
  Michigan State University/Joint Institute for Nuclear
  Astrophysics/American Astronomical Society press release, {\it forthcoming}

\end{itemize}


\section{Code Description and Characterization}

\texttt{FLASH} is a highly capable, fully modular, extensible, community
code that is widely used in astrophysics, cosmology, fluid dynamics, and
plasma physics, and other fields.  The capabilities of the FLASH code
include adaptive mesh refinement (AMR), several self-gravity solvers, an
advection-diffusion-reaction (ADR) flame model, an accurate and
detailed treatment of nuclear burning, and a sophisticated two-moment
neutrino transport scheme based on an explicit hyperbolic solver.

\texttt{FLASH} is written in modern Fortran, with some utility functions
written in C, and a build system written in Python.  It requires MPI
library support, and either HDF5 or P-NetCDF for I/O.  Additional
mathematical, such as \texttt{Hypre}, may be required to configure
\texttt{FLASH} for particular simulations.

Algorithm classes used within \texttt{FLASH} include Sparse Linear Algebra
solvers, FFT, active and passive particles, structured grids, and AMR.

\end{document}
