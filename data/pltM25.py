#!/usr/bin/env python                                                                                                                                                                  

from numpy import *
from pylab import *
from string import *
from matplotlib.ticker import AutoMinorLocator

radius, dens, velz, lmri, omri, rmri = loadtxt('m25_371ms.1d', usecols=(0,1,2,3,4,5), unpack=True)

figure(1,figsize=(5,3.5))
rc('lines',markeredgewidth=1.)
rc('text', usetex=True)
rc('font', family='serif', size=12)
ax1 = subplot(111)
ax2 = ax1.twinx()

ax1.semilogy()
ax2.semilogy()
#ax1.loglog()
#ax2.loglog()

minorLocatorX   = AutoMinorLocator()
minorLocatorY   = AutoMinorLocator()
minorLocatorY2   = AutoMinorLocator()

###  Let's double the rotation rate and increase B_init.
#velz = 2.*velz
#lmri = 0.5*lmri
#omri = 2.*omri
###

magb = 2.e12*(dens/5e12)**0.25
bsat = velz*(0.5*8*3.14159265/5*dens)**0.5

##
#magb = 0.1 * bsat
##

lines = []
lines.extend(ax1.plot(radius/1e5,velz/radius,'k-',linewidth=1.5))
lines.extend(ax2.plot(radius[70:]/1e5,(1./omri[70:]), 'm-',linewidth=1.5))
#lines.extend(ax2.plot(radius[70:]/1e5, lmri[70:]*magb[70:]/1.e5, 'b-',linewidth=1.5))
lines.extend(ax1.plot(radius/1e5, magb/1e10, 'b-', linewidth=1.5))
lines.extend(ax2.plot(radius[70:]/1e5, lmri[70:]*magb[70:]/1.e5/10, 'r-',linewidth=1.5))
lines.extend(ax2.plot((40,60), (0.03, 0.03), 'g--',linewidth=1.5))
lines.extend(ax1.plot(radius/1e5,bsat/1e13, 'c-',linewidth=1.5))
ax2.plot((60,75), (0.06, 0.06), 'g--',linewidth=1.5)
ax2.plot((75,90), (0.125,0.125),'g--',linewidth=1.5)
ax2.plot((90,150), (0.25,0.25),'g--',linewidth=1.5)
ax2.plot((10,40), (0.25,0.25),'g--',linewidth=1.5)
#ax1.set_xticks([512,1024,2048,4096,8192,16384])
#ax2.set_xticks([512,1024,2048,4096,8192,16384])
ax1.tick_params(width=1.,which='both')
ax2.tick_params(width=1.,which='both')
ax1.xaxis.set_minor_locator(minorLocatorX)
#ax1.yaxis.set_minor_locator(minorLocatorY)
#ax2.yaxis.set_minor_locator(minorLocatorY2)

ax1.set_xlim((12,150.))
ax2.set_ylim((1e-3, 10))
ax1.set_ylim((1, 1000))
    
ax1.set_xlabel(r'Radius [km]')
ax2.set_ylabel(r'$\tau_{\rm MRI}\ [{\rm s}]$, $\lambda_{\rm MRI}/5\ {\rm [km]}$, $dx_{\rm sim}\ {\rm [km]}$')
ax1.set_ylabel(r'$\Omega\ [{\rm rad\ s^{-1}}],\ B_{\rm sat}\ {\rm [10^{13}\ G]}\ B_{\rm init}\ [{\rm 10^{10}\ G}]$')
#ax1.set_xlabel('Mira nodes (16 cores/node)')
#ax1.set_ylabel('1e-8 core-hours/zone/step')

subplots_adjust(bottom=0.12, right=0.85)
gca().xaxis.set_label_coords(0.5,-0.08)

#lab1 = r'${\rm Total evolution}$'
#lab2 = r'${\rm Hydro}$'
lab1 = r'$\Omega$'
lab2 = r'$\tau_{\rm MRI}$'
lab3 = r'$B_{\rm init}$'
lab4 = r'$\lambda_{\rm MRI}/10$'
lab5 = r'$dx_{\rm sim}$'
lab6 = R'$B_{\rm sat}$'
legend(lines,(lab1,lab2,lab3,lab4,lab5,lab6), 'lower left',labelspacing=0.05,handletextpad=0.,ncol=2, columnspacing=0.05)
leg = gca().get_legend()
leg.draw_frame(False)
ltext = gca().get_legend().get_texts()
setp(ltext, fontsize=10)

savefig("m25.pdf")
