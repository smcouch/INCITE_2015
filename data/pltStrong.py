#!/usr/bin/env python                                                                                                                                                                  

from numpy import *
from pylab import *
from string import *
from matplotlib.ticker import AutoMinorLocator

ranks, blocks, evol, hydro, grav, plt, grid = loadtxt('strPerf_cube16.dat', usecols=(0,1,2,4,5,7,8), unpack=True)

steps = 50.
nxb = 16.
nodes = 512

cstEvol = ranks*8*evol/3600./(blocks*nxb**3*steps)
cstHydr = ranks*8*hydro/3600./(blocks*nxb**3*steps)
cstGrav = ranks*8*grav/3600./(blocks*nxb**3*steps)
cstPlot = ranks*8*plt/3600./(blocks*nxb**3)
cstGrid = ranks*8*grid/3600./(blocks*nxb**3*steps)

#suEvol = cstEvol[0] / cstEvol
#suHydr = cstHydr[0] / cstHydr
#suGrav = cstGrav[0] / cstGrav 
#suPlot = cstPlot[0] / cstPlot
#suGrid = cstGrid[0] / cstGrid 

figure(1,figsize=(5,3.5))
rc('lines',markeredgewidth=1.)
rc('text', usetex=True)
rc('font', family='serif', size=12)
ax1 = subplot(111)
#ax2 = ax1.twinx()

#ax1.semilogx()
#ax2.semilogx()
ax1.loglog()
#ax2.loglog()

minorLocatorX   = AutoMinorLocator()
minorLocatorY   = AutoMinorLocator()
#minorLocatorY2   = AutoMinorLocator()

lines = []
lines.extend(ax1.plot(ranks, evol, 'b-',linewidth=1.5))
lines.extend(ax1.plot(ranks, hydro, 'r-',linewidth=1.5))
lines.extend(ax1.plot(ranks, grav, 'm-',linewidth=1.5))
lines.extend(ax1.plot(ranks, grid, 'c-',linewidth=1.5))
lines.extend(ax1.plot(ranks, plt, 'g-',linewidth=1.5))
ax1.plot(ranks, evol, 'bs')
ax1.plot(ranks, hydro, 'rs')
ax1.plot(ranks, grav, 'ms')
ax1.plot(ranks, grid, 'cs')
ax1.plot(ranks, plt, 'gs')
lines.extend(ax1.plot(ranks, evol[0]*512/ranks, 'k--',linewidth=1.5))
ax1.plot(ranks, evol[0]*512/ranks, 'b--',linewidth=1.5)
ax1.plot(ranks, hydro[0]*512/ranks, 'r--',linewidth=1.5)
ax1.plot(ranks, grav[0]*512/ranks, 'm--',linewidth=1.5)
ax1.plot(ranks, grid[0]*512/ranks, 'c--',linewidth=1.5)
#ax1.plot((512,16384), (suHydr[0],suHydr[0]), 'k--')

#ax1.set_xscale('log')
ax1.minorticks_off()
ax1.set_xticks([512,1024,2048,4096])
ax1.get_xaxis().set_major_formatter(matplotlib.ticker.ScalarFormatter())
#ax1.set_xticklabels(["512\n(4k)", "1024\n(8k)", "2048\n(16k)", "4096\n(32k)", "8192\n(64k)", "16384\n(128k)", "32768\n(256k)"])

#ax1.set_xticks([512,1024,2048,4096])
#ax2.set_xticks([512,1024,2048,4096,8192,16384])
#ax1.tick_params(width=1.,which='both')
#ax1.xaxis.set_minor_locator(minorLocatorX)
#ax1.yaxis.set_minor_locator(minorLocatorY)
#ax2.yaxis.set_minor_locator(minorLocatorY2)

ax1.set_ylim( (2, 10000))
ax1.set_xlim((400, 5000))

ax1.set_xlabel(r'Ranks (9216 AMR blocks)')
#ax2.set_ylabel(r'$10^{-7}$ core hours/zone/IO write')
#ax1.set_xlabel('Mira nodes (16 cores/node)')
ax1.set_ylabel(r'Time to solution [s]')

subplots_adjust(bottom=0.12)
gca().xaxis.set_label_coords(0.5,-0.08)

#lab1 = r'${\rm Total evolution}$'
#lab2 = r'${\rm Hydro}$'
lab1 = 'Total evolution'
lab2 = 'MHD'
lab3 = 'Gravity'
lab4 = 'Grid'
lab5 = 'IO (1 write)'
lab6 = 'Ideal'
legend(lines,(lab1,lab2,lab3,lab4,lab5,lab6), 'upper right',labelspacing=0.05,handletextpad=0.)
leg = gca().get_legend()
leg.draw_frame(False)
ltext = gca().get_legend().get_texts()
setp(ltext, fontsize=10)

savefig("strScaling.pdf")
