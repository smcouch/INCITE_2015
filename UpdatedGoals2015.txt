The goals for the first year of our project, "Petascale Simulation of
Magnetorotational Core-Collapse Supernovae," will remain largely
unchanged despite the reduction in awarded hours.  In order to
accomodate the reduced resources, we will run the primary "MRI"
simulation for half as long as originally proposed.  This is still
much greater than the estimated MRI growth time in the CCSN context,
allowing us to still directly address questions related to the
development and saturation of the MRI.  Importantly, by choosing to
not run with reduced resolution, this leaves open the possibility to
continue the high-resolution MRI simulation in future years of the
project, if resources are granted to do so.  Also, we will decrease
the number of full neutrino transport 3D supernova simulations from
two to one, in order to accomodate the reduced allocation.  Even one
full transport 3D simulation is a significant scientific contribution.
Furthermore, we will run this simulation for 400 ms of post-bounce
time instead of 500 ms which will allow us to run two additional 3D
full transport simulations in a reduced 3D octant geometry.  These
simulations will cost about one-eighth of the full 3D simulation but
will still be interesting and useful.

Also, in the first three months of 2015, we plan to work closely with
our Catalyst in order to profile and optimize our code even further,
particularly the M1 transport component.  If successful, optimization
of our code may allow us to run the simulations for longer times,
bringing us even closer to the original goals outlined in the
proposal.

The updated resource usage for the MRI simulations will be 30M Mira
service units, and the updated resource usage for the full transport
3D M1 simulations will be 20M Mira service units.
