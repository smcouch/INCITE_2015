#!/usr/bin/env python                                                                                                                                                                  

from numpy import *
from pylab import *
from string import *
from matplotlib.ticker import AutoMinorLocator

threads, evol, hydro, grav, plt, grid = loadtxt('thrPerf_cube16.dat', usecols=(0,1,2,3,5,6), unpack=True)

steps = 50.
nxb = 16.
blocks = 16384.
nodes = 512

cstEvol = nodes*16*evol/3600./(blocks*nxb**3*steps)
cstHydr = nodes*16*hydro/3600./(blocks*nxb**3*steps)
cstGrav = nodes*16*grav/3600./(blocks*nxb**3*steps)
cstPlot = nodes*16*plt/3600./(blocks*nxb**3)
cstGrid = nodes*16*grid/3600./(blocks*nxb**3*steps)

suEvol = cstEvol[0] / cstEvol
suHydr = cstHydr[0] / cstHydr
suGrav = cstGrav[0] / cstGrav 
suPlot = cstPlot[0] / cstPlot
suGrid = cstGrid[0] / cstGrid 

figure(1,figsize=(5,3.5))
rc('lines',markeredgewidth=1.)
rc('text', usetex=True)
rc('font', family='serif', size=12)
ax1 = subplot(111)
#ax2 = ax1.twinx()

#ax1.semilogx()
#ax2.semilogx()
#ax1.loglog()
#ax2.loglog()

minorLocatorX   = AutoMinorLocator()
minorLocatorY   = AutoMinorLocator()
#minorLocatorY2   = AutoMinorLocator()

lines = []
lines.extend(ax1.plot(threads, suEvol, 'b-',linewidth=1.5))
lines.extend(ax1.plot(threads, suHydr, 'r-',linewidth=1.5))
lines.extend(ax1.plot(threads, suGrav, 'm-',linewidth=1.5))
lines.extend(ax1.plot(threads, suGrid, 'c-',linewidth=1.5))
lines.extend(ax1.plot(threads, suPlot, 'g-',linewidth=1.5))
ax1.plot(threads, suEvol, 'bs')
ax1.plot(threads, suHydr, 'rs')
ax1.plot(threads, suGrav, 'ms')
ax1.plot(threads, suGrid, 'cs')
ax1.plot(threads, suPlot, 'gs')
#lines.extend(ax1.plot((1,8), (1,4), 'k--'))
#ax1.plot((512,16384), (suHydr[0],suHydr[0]), 'k--')


#ax1.set_xticks([512,1024,2048,4096,8192,16384])
#ax2.set_xticks([512,1024,2048,4096,8192,16384])
#ax1.tick_params(width=1.,which='both')
#ax1.xaxis.set_minor_locator(minorLocatorX)
ax1.yaxis.set_minor_locator(minorLocatorY)
#ax2.yaxis.set_minor_locator(minorLocatorY2)

ax1.set_ylim( (.5, 4))
ax1.set_xlim((.8, 8.2))

ax1.set_xlabel(r'Threads/rank (512 nodes, 8 ranks/node)')
ax1.set_ylabel(r'Thread-to-thread speedup')
#ax2.set_ylabel(r'$10^{-7}$ core hours/zone/IO write')
#ax1.set_xlabel('Mira nodes (16 cores/node)')
#ax1.set_ylabel('1e-8 core-hours/zone/step')

subplots_adjust(bottom=0.12)
gca().xaxis.set_label_coords(0.5,-0.08)

#lab1 = r'${\rm Total evolution}$'
#lab2 = r'${\rm Hydro}$'
lab1 = 'Total evolution'
lab2 = 'MHD'
lab3 = 'Gravity'
lab4 = 'Grid'
lab5 = 'IO'
lab6 = 'Ideal'
legend(lines,(lab1,lab2,lab3,lab4,lab5,lab6), 'upper left',labelspacing=0.05,handletextpad=0.)
leg = gca().get_legend()
leg.draw_frame(False)
ltext = gca().get_legend().get_texts()
setp(ltext, fontsize=10)

savefig("thrSpeedup.pdf")
