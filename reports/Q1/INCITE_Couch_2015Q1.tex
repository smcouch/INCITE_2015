\documentclass[12pt,titlepage]{article}

\setlength{\oddsidemargin}{0in}
\setlength{\evensidemargin}{0in}
\setlength{\textwidth}{6.5in}
%
\setlength{\textheight}{9in}
\setlength{\topmargin}{0in}
\setlength{\headsep}{0in}
\setlength{\topskip}{0in}
\setlength{\headheight}{0in}

\usepackage{graphicx}
\usepackage[square,comma,authoryear]{natbib}


\title{DOE Office of Science INCITE Project:\\
Petascale Simulation of Magnetorotational Core-Collapse Supernovae\\
Q1 2015 Report}

\author{Principal Investigator:\\Sean M. Couch\\Theoretical
  AstroPhysics Including Relativity\\California Institute of Technology}

\date{April 1, 2015}

\begin{document}


\maketitle

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Report on Project Milestones}

%%%%%%%%%%%%%%%%%%%

Core-collapse supernovae (CCSNe) are the luminous explosions that
herald the death of massive stars.  Neutron stars, pulsars, magnetars,
and stellar-mass black holes are all born out of these explosions.
Some Gamma-Ray Bursts (GRBs) have been associated with CCSNe, raising
the possibility of a common progenitor for both.  CCSNe are
responsible for the production of many elements throughout the
universe, especially those heavier than iron; their importance in
galactic chemical evolution cannot be underestimated.  Despite the
importance of CCSNe to our understanding of many aspects of the
universe the mechanism that reverses stellar core collapse and drives
supernova explosions is not fully understood leaving our understanding
of many important astrophysical phenomena incomplete.  

This project is a comprehensive study of the impact of rotation and
magnetic fields on core-collapse supernovae. We will carry out a
series of 3D magnetohydrodynamics (MHD) simulations of the collapse of
rotating, magnetic stellar cores using the FLASH multi-physics,
adaptive mesh refinement (AMR) simulation framework.  We are including
in our simulations realistic treatments for neutrino physics,
realistic progenitor rotation and magnetic fields, and sufficient
resolution to capture the growth of the magnetorotational instability
(MRI).  We will use these simulations to quantify how much rotational
energy of the progenitor cores can be tapped to aid neutrinos in
driving successful and robust explosions. 

Our revised goals for Year 1 comprise two primary science targets.
First, we will study the influence of magnetohydrodynamic (MHD)
turbulence on the CCSN mechanism with an extremely-high resolution 3D
simulation using approximate neutrino transport methods.  During Q1,
our principle milestone relating to this project goal was to conduct a
series of lower-resolution ``pilot'' simulations for use in tuning the
initial conditions and code for the one, full-scale simulation.  The
second goal for Year 1 is to carry out moderate resolution simulations
of CCSNe using high-fidelity neutrino transport methods in 3D.  The
principle milestone for the neutrino transport simulations in Q1 was
to enhance the performance and scalability of the code.

\subsection{Milestone: MHD ``Pilot'' Simulations}

\begin{figure}
  \centering
  \includegraphics[width=3.5in]{plasmaBeta_o110b9_125mV500m_grid_0393_cropped}
  \caption{Equatorial slice plots of the plasma beta parameter from
    two MHD CCSN simulations conducted at differing resolutions. Blue
    colors indicate strong fields while yellow colors represent weaker
    fields. The right half shows the grid structure for the moderate
    resolution case that will be the basis for the full, high
    resolution run in Q2. }
  \label{fig:beta}
\end{figure}

So far in Q1 of 2015, we have ran several MHD CCSN simulations in
which we have varied the initial rotation rate of the progenitor star
and initial magnetic field strength.  Most of these simulations have
used a ``standard'' finest grid spacing of 500 m.  For two of these
simulations, we have added additional levels of refinement about 40 ms
after core bounce to achieve finest grid spacings of 125 m.  As
described in our proposal, these higher resolution grid elements are
only placed beyond the central proto-neutron star in the region
susceptible to the MRI.

An example and comparison of the grid structures between MHD
simulations of differing resolutions is shown in Figure \ref{fig:beta}
where we plot the ratio of gas pressure to magnetic pressure (the
so-called ``plasma beta'' parameter).  The simulations we have run so
far, such as those shown in Figure \ref{fig:beta}, have verified our
initial expectations of the regions susceptible to growth of the MRI
and also our expectations that magnetic field amplification would
increase with finer resolution.  We are now confident and ready to
proceed to the full production simulation which will start from
similar initial conditions to those shown in Figure \ref{fig:beta} but
with an additional level of refinement.  This simulation will have a
finest grid spacing of about 63 m outside of the proto-neutron star.
This simulation will be started early in Q2 and will consume the
majority of our allocation for Year 1.

\subsection{Milestone: Optimization of Neutrino Transport Code}

\begin{figure}
  \centering
  \includegraphics[width=3.5in]{perf}
  \caption{Computational performance of the enhance M1 neutrino
    transport code.  The top panel shows thread-to-thread speed up for
    several choices of block size.  The bottom panel shows the real
    cost as a function of thread count for the same set of block
    sizes.  Compared to the code at the time of proposal submission,
    our efforts have realized about a factor of two speed up in code
    performance. }
  \label{fig:perf}
\end{figure}

Concurrently during Q1, we have spent a great deal of effort
optimizing the neutrino transport variant of our code.  As described
in our proposal, we use a fully-explicit hyperbolic approach to
solving the system of neutrino transport equations.  This approach is
computationally efficient for the CCSN problem, though like all
transport methods is memory-intensive and requires substantial
communication overhead.  In order increase the performance and
scalability of our transport code, we focussed on two areas of code
improvement.  First, we focussed on the raw performance of the
algorithm on the BG/Q architecture by improving the use of vectorized
operations and reducing cache misses.  Second, we improved the code's
parallel performance by increasing the size of the adaptive mesh
refinement unit block used in our runs from $16^3$ to $24^3$ or
$32^3$.  This had multiple beneficial effects.  This increased the
ratio of interior zones to needed ghost zones, hence reducing the
communication cost per zone per step. Second, it improved OpenMP
thread-to-thread speed up by increasing the amount of work necessary
per block per stop (our code is threaded over zones within the block).

The result of our code tuning is shown graphically in Figure
\ref{fig:perf} where we plot the thread-to-thread speed up (top) and
the total usage rate of the code as a function of the threads per MPI
rank (bottom).  For the bottom panel, the total number of execution
threads per BG/Q node is fixed at 64 for all thread counts of 4 or
more.  Do to memory requirements of our transport code, full
production simulations require at least 8 OpenMP threads per MPI rank
so that sufficient memory is available to each MPI rank.  Figure
\ref{fig:perf} shows that our efforts have resulted in a real
performance increase of about a factor of two compared with the code's
performance at the time of the submission of our proposal.  These
results were compiled from ones on one rack of BG/Q and we are now
conducting a weak scaling test with the enhance transport code to
verify improved scaling performance.  Following these final tests, we
expect to start our production transport simulations during Q2.  Our
revised goals target just one such simulation, but given the
improvements we have made to the code's performance it should be
feasible to conduct two such simulations within our Year 1 allocation. 



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Project Productivity}
%%%%%%%%%%%%%%%%%%%
\subsection{Primary Products}

{\bf Publications}
\begin{itemize}

\item Couch, S.M., Ott, C.D., ``The Role of Turbulence in
  Neutrino-driven Core-collapse Supernova Explosions,'' {\it
    Astrophysical Journal}, 799, 5

\item Radice, D., Couch, S.M., Ott, C.D., ``Implicit Large Eddy
  Simulations of Anisotropic Weakly Compressible Turbulence with
  Application to Core-collapse Supernovae,'' submitted to {\it
    Computational Astrophysics and Cosmology}, arXiv:1501:3169

\item Couch, S.M., Chatzopoulos, E., Arnett, W.D., Timmes, F.X., ``The
  Three Dimensional Evolution to Core Collapse of a Massive Star,''
  submitted to {\it Astrophysical Journal Letters}, arXiv:1503.2199

\end{itemize}

\noindent {\bf Presentations}

\begin{itemize}

\item Couch, S.M., ``Turbulent Frontiers in Massive Stellar Death,''
  Physics Colloquium, North Carolina State University, Raleigh, NC,
  January 12, 2015

\item Couch, S.M., ``Turbulent Frontiers in Massive Stellar Death,''
  Physics Colloquium, University of Washington, Seattle, WA,
  January 26, 2015

\item Couch, S.M., ``Turbulent Frontiers in Massive Stellar Death,''
  Astronomy Seminar, Michigan State University, East Lansing, MI,
  February 4, 2015

\end{itemize}

\subsection{Secondary}

During Q1 of 2015, PI Couch was offered and accepted a position as an
Assistant Professor in the Departments of Physics \& Astronomy and
Computational Math, Science, \& Engineering at Michigan State
University. Without doubt, this INCITE project played a role in this
achievement. 

\section{Code Description and Characterization}

\texttt{FLASH} is a highly capable, fully modular, extensible, community
code that is widely used in astrophysics, cosmology, fluid dynamics, and
plasma physics, and other fields.  The capabilities of the FLASH code
include adaptive mesh refinement (AMR), several self-gravity solvers, an
advection-diffusion-reaction (ADR) flame model, an accurate and
detailed treatment of nuclear burning, and a sophisticated two-moment
neutrino transport scheme based on an explicit hyperbolic solver.

\texttt{FLASH} is written in modern Fortran, with some utility functions
written in C, and a build system written in Python.  It requires MPI
library support, and either HDF5 or P-NetCDF for I/O.  Additional
mathematical, such as \texttt{Hypre}, may be required to configure
\texttt{FLASH} for particular simulations.

Algorithm classes used within \texttt{FLASH} include Sparse Linear Algebra
solvers, FFT, active and passive particles, structured grids, and AMR.

\end{document}
